# Personal Repo

## Q&A

### How can I add this repo in my Arch based distro?
Add this to your pacman.conf;
```
[quitaxd-repo]
SigLevel = Optional TrustAll
Server = https://gitlab.com/Quitaxd/personal_repo/-/raw/main/$arch
```